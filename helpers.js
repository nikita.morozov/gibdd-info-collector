const webdriver = require('selenium-webdriver');
const { By, until, Key } = webdriver;

const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');
const http = require('http');
const https = require('https');
const URL = require('url');
const ProgressBar = require('progress');
const EventEmitter = require('events');

async function SendValueToField (driver, filed, value, scrolling = true) {
  try {
    await driver.wait(until.elementLocated(By.xpath(filed))).then(async element => {
      await driver.wait(until.elementIsEnabled(element)).then(async () => {
        if (!!scrolling) {
          await ScrollToElement(driver, filed);
        }
        await driver.findElement(By.xpath(filed)).sendKeys(value);
      });
    });
  } catch (e) {
    await console.log('SENDING ERROR');
  }
}

async function SendToAttributeValue (driver, xpath, value) {
  try {
    await driver.wait(until.elementLocated(By.xpath(xpath))).then(async element => {
      await driver.wait(until.elementIsEnabled(element)).then(async () => {
        await ScrollToElement(driver, xpath);
        await driver.findElement(By.xpath(xpath)).setAttribute('value', value);
      });
    });
  } catch (e) {
    await console.log('SENDING ERROR');
  }
}

async function ClickToObject (driver, obj, scrolling = true) {
  try {
    await driver.wait(until.elementLocated(By.xpath(obj))).then(async element => {
      await driver.wait(until.elementIsEnabled(element)).then(async () => {
        if (!!scrolling) {
          await ScrollToElement(driver, obj);
        }
        await driver.findElement(By.xpath(obj)).click();
      });
    });
  } catch (e) {
    await console.log('CLICKING ERROR');
  }
}

async function WaitWhileNotDisplayed (driver, xpath, scrolling = true) {
  await driver.wait(webdriver.until.elementLocated(By.xpath(xpath))).then(async () => {
    let displayed = await driver.findElement(By.xpath(xpath)).isDisplayed();
    await console.log('DISPLAYED: ', displayed);
    await driver.sleep(500);
    if (!!displayed) {
      if (!!scrolling) {
        await ScrollToElement(driver, xpath);
      }
      await driver.sleep(1000);
      await driver.wait(webdriver.until.elementLocated(By.xpath(xpath))).then(async element => {
        await driver.wait(webdriver.until.elementIsEnabled(element)).then(async () => {
          await driver.wait(webdriver.until.elementIsNotSelected(element)).then(async () => {
            await console.log('IS DISPLAYED');
          });
        });
      });
    } else {
      await driver.sleep(500);
      await WaitWhileNotDisplayed(driver, xpath);
    }
  });
}

async function WaitWhileDisplayed (driver, xpath, useSpecLog = false, trueLog = '', falseLog = '', useRefreshing = false) {
  let displayed = false;
  let counter = 0;
  let refreshing = 0;
  do {
    try {
      await driver.wait(webdriver.until.elementLocated(By.xpath(xpath)), 2000).then(async () => {
        displayed = await driver.findElement(By.xpath(xpath)).isDisplayed();
        await driver.sleep(500);
        await ScrollToElement(driver, xpath);
        await console.log((!useSpecLog) ? 'ELEMENT IS DISPLAYED' : trueLog);
        if (!!useRefreshing) {
          counter += 1;
          if (counter === 50) {
            if (refreshing <= 3) {
              await console.log('REFRESHING PAGE WHILE WAITING');
              await driver.navigate().refresh();
              counter = 0;
              refreshing = +1;
            } else {
              await console.log('REFRESHING NOT COMPLETE');
              displayed = false;
            }
          }
        }
      });
    } catch (e) {
      await console.log((!useSpecLog) ? 'ELEMENT NOT FOUND IN DOM' : falseLog);
      displayed = false;
    }
  } while (!!displayed);
}

async function WaitElementLocated (driver, xpath) {
  await driver.wait(webdriver.until.elementLocated(By.xpath(xpath)));
  await console.log('ELEMENT LOCATED IN DOM');
}

async function DeleteElementFromDOM (driver, element) {
  await driver.wait(webdriver.until.elementLocated(By.xpath(element)), 5000).then(async () => {
    let deletingElement = await driver.findElement(By.xpath(element));
    await driver.executeScript('arguments[0].remove()', await deletingElement);
    await console.log('ELEMENT DELETED FROM DOM');
  });
}

async function GetDataToArray (driver, xpath, ifNull) {
  let returningArray = [];

  await driver.wait(until.elementLocated(By.xpath(xpath)), 3000).then(async element => {
    await ScrollToElement(driver, xpath);
    await driver.wait(until.elementIsEnabled(element)).then(async () => {
      await driver.findElements(By.xpath(xpath)).then(async elements => {
        await elements.map(async (item, index, arr) => {
          try {
            let itemTxt = await item.getText();
            if (!await itemTxt) {
              itemTxt = await item.getAttribute('value');
            }
            if (!!ifNull && !await itemTxt) {
              itemTxt = '-';
            }
            await console.log('RETURNING VALUE: ', await itemTxt);
            await returningArray.push(await itemTxt);
          } catch (err) {
            await console.log('GET DATA TO ARRAY ERROR: ', await err);
          }
        });
      });
    });
  });
  return await returningArray;
}

async function ScrollToElement (driver, xpath, pause = 300) {
  let element = await driver.findElement(By.xpath(xpath));
  await driver.executeScript('arguments[0].scrollIntoView()', element);
  await driver.sleep(pause);
}

async function TakeElementScreensShot (driver, xpath, fileName = 'image.png') {
  let element;
  await driver.wait(until.elementLocated(By.xpath(xpath))).then(async () => {
    element = await driver.findElement(By.xpath(xpath));
    await ScrollToElement(driver, xpath);
  });
  await element.takeScreenshot().then(async function (image, err) {
    await require('fs').writeFile(fileName, image, 'base64', async function () {
      if (!err) {
        // image.toString('base64');
        await console.log('DTP SCREEN SAVED');
      } else await console.log('ERROR: ', err);
    });
  });
}

async function ChangeTaskItemValue (taskDB, id, vin, status, error, carData, ownership, checkDate, DTP, wanted, restrictions, customs, standard, mos) {
  let baseID = await taskDB.get(`task${id}.id`);
  let baseVIN = await taskDB.get(`task${id}.vin`);
  let baseCarData = await taskDB.get(`task${id}.carData`);
  let baseOwnershipPeriod = await taskDB.get(`task${id}.ownershipPeriod`);
  let baseDTP = await taskDB.get(`task${id}.DTP`);
  let baseWanted = await taskDB.get(`task${id}.wanted`);
  let baseRestrictions = await taskDB.get(`task${id}.restrictions`);
  let baseCustoms = await taskDB.get(`task${id}.customs`);
  let baseStandard = await taskDB.get(`task${id}.standard`);
  let baseMos = await taskDB.get(`task${id}.mos`);
  let baseCheckDate = await taskDB.get(`task${id}.checkDate`);
  let baseError = await taskDB.get(`task${id}.error`);
  let baseStatus = await taskDB.get(`task${id}.status`);

  await taskDB.set(`task${id}`, {
    id: (!!id && id !== baseID) ? id : (!!baseID) ? baseID : undefined,
    vin: (!!vin && vin !== baseVIN) ? vin : (!!baseVIN) ? baseVIN : undefined,
    carData: (!!carData && carData !== baseCarData) ? carData : (!!baseCarData) ? baseCarData : undefined,
    ownershipPeriod: (!!ownership && ownership !== baseOwnershipPeriod) ? ownership : (!!baseOwnershipPeriod) ? baseOwnershipPeriod : undefined,
    DTP: (!!DTP && DTP !== baseDTP) ? DTP : (!!baseDTP) ? baseDTP : undefined,
    wanted: (!!wanted && wanted !== baseWanted) ? wanted : (!!baseWanted) ? baseWanted : undefined,
    restrictions: (!!restrictions && restrictions !== baseRestrictions) ? restrictions : (!!baseRestrictions) ? baseRestrictions : undefined,
    customs: (!!customs && customs !== baseCustoms) ? customs : (!!baseCustoms) ? baseCustoms : undefined,
    standard: (!!standard && standard !== baseStandard) ? standard : (!!baseStandard) ? baseStandard : undefined,
    mos: (!!mos && mos !== baseMos) ? mos : (!!baseMos) ? baseMos : undefined,
    checkDate: (!!checkDate && checkDate !== baseCheckDate) ? checkDate : (!!baseCheckDate) ? baseCheckDate : undefined,
    error: (!!error && error !== baseError && status === 'error') ? error : (!!baseError && status === 'error') ? baseError : undefined,
    status: (!!status && status !== baseStatus) ? status : (!!baseStatus) ? baseStatus : undefined,
  });
}

async function ChangeTaskItemValue2 (taskDB, id, vin, status, type, data1) {
  let baseID = await taskDB.get(`task${id}.id`);
  let baseVIN = await taskDB.get(`task${id}.vin`);
  let baseCarData = await taskDB.get(`task${id}.carData`);
  let baseOwnershipPeriod = await taskDB.get(`task${id}.ownershipPeriod`);
  let baseDTP = await taskDB.get(`task${id}.DTP`);
  let baseWanted = await taskDB.get(`task${id}.wanted`);
  let baseRestrictions = await taskDB.get(`task${id}.restrictions`);
  let baseCustoms = await taskDB.get(`task${id}.customs`);
  let baseStandard = await taskDB.get(`task${id}.standard`);
  let baseMos = await taskDB.get(`task${id}.mos`);
  let baseNotary = await taskDB.get(`task${id}.notary`);
  let baseCheckDate = await taskDB.get(`task${id}.checkDate`);
  let baseError = await taskDB.get(`task${id}.error`);
  let baseStatus = await taskDB.get(`task${id}.status`);
  await taskDB.set(`task${id}`, {
    id: (!!id && id !== baseID) ? id : (!!baseID) ? baseID : undefined,
    vin: (!!vin && vin !== baseVIN) ? vin : (!!baseVIN) ? baseVIN : undefined,
    carData: (!!data1 && data1 !== baseCarData && type === 'history') ? data1 : (!!baseCarData) ? baseCarData : undefined,
    ownershipPeriod: (!!data1 && data1 !== baseOwnershipPeriod && type === 'ownership') ? data1 : (!!baseOwnershipPeriod) ? baseOwnershipPeriod : undefined,
    DTP: (!!data1 && data1 !== baseDTP && type === 'dtp') ? data1 : (!!baseDTP) ? baseDTP : undefined,
    wanted: (!!data1 && data1 !== baseWanted && type === 'wanted') ? data1 : (!!baseWanted) ? baseWanted : undefined,
    restrictions: (!!data1 && data1 !== baseRestrictions && type === 'restrictions') ? data1 : (!!baseRestrictions) ? baseRestrictions : undefined,
    customs: (!!data1 && data1 !== baseCustoms && type === 'customs') ? data1 : (!!baseCustoms) ? baseCustoms : undefined,
    standard: (!!data1 && data1 !== baseStandard && type === 'standard') ? data1 : (!!baseStandard) ? baseStandard : undefined,
    mos: (!!data1 && data1 !== baseMos && type === 'mos') ? data1 : (!!baseMos) ? baseMos : undefined,
    notary: (!!data1 && data1 !== baseNotary && type === 'notary') ? data1 : (!!baseNotary) ? baseNotary : undefined,
    checkDate: (!!data1 && data1 !== baseCheckDate && type === 'checkDate') ? data1 : (!!baseCheckDate) ? baseCheckDate : undefined,
    error: (!!data1 && data1 !== baseError && type === 'error' && status === 'error') ? data1 : (!!baseError && status === 'error') ? baseError : undefined,
    status: (!!status && status !== baseStatus) ? status : (!!baseStatus) ? baseStatus : undefined,
  });
}

function Sleep (ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

/**
 * @return {string}
 */
async function CreateCollectionID () {
  let date = new Date();
  let month = await date.getMonth() + 1;
  let collectionID = await date.getDate() + '' + await month + '' + await date.getFullYear() + '' + await date.getHours() + '' + await date.getMinutes() + '' + await date.getSeconds();
  return collectionID;
}

async function FirstDriverAction (driver, taskDB, id, vin, VINField, checkBTN, whileChecking, checkingError, checkingResult, checkingResultNotFound) {
  await ChangeTaskItemValue(taskDB, id, vin, 'processing');
  await driver.sleep(4000);
  await SendValueToField(driver, VINField, vin);
  await driver.sleep(2000);
  // await ClickingOnButton(driver, taskDB, id, vin, checkBTN, whileChecking, checkingError, checkingResult, checkingResultNotFound, 3);
  await ClickToButtonAndCheckData(driver, taskDB, id, vin, checkBTN);
  if (await taskDB.get(`task${id}.status`) === 'error' || await taskDB.get(`task${id}.status`) === 'receiving') {
    await console.log('STATUS IS ERROR OR RECEIVING');
    return;
  }

  await ChangeTaskItemValue(taskDB, id, vin, 'receiving');
}

async function ClickingOnButton (driver, taskDB, id, vin, button, whileChecking, checkingError, checkingResult, checkingResultNotFound) {
  await ClickToObject(driver, button);
  await ChangeTaskItemValue(taskDB, id, vin, 'waiting');
  await WaitWhileDisplayed(driver, whileChecking, true, 'REQUEST IN PROGRESS, WAIT...', 'REQUEST IS COMPLETE');
  try {
    let errorText = await driver.findElement(By.xpath(checkingError)).getText();
    await console.log('CHECKING ERROR: ', await errorText);
    await ChangeTaskItemValue(taskDB, id, vin, 'error', await errorText);
    return;
  } catch (e) {
    await console.log('ERROR, NOT VISIBLE');
    try {
      let errorText = await driver.findElement(By.xpath(`//*[contains(text(), 'Google reCaptcha v3 не была пройдена')]`)).getText();
      await console.log('CAPTCHA NOT SOLVED: ', await errorText);
      await ChangeTaskItemValue(taskDB, id, vin, 'error', await errorText);
      return;
    } catch (e) {
      await console.log('CAPTCHA ERROR, NOT VISIBLE');
    }
  }
  // await WaitWhileNotDisplayed(driver, checkingResult);
  try {
    await driver.sleep(1500);
    await driver.wait(until.elementLocated(By.xpath(checkingResultNotFound)), 5000).then(async () => {
      let resultText = await driver.findElement(By.xpath(checkingResultNotFound)).getText();
      await console.log('CHECKING RESULT: ', await resultText);
      await ChangeTaskItemValue2(taskDB, id, vin, 'collected', undefined, await resultText);
    });
    return;
  } catch (e) {
    await console.log('CHECKING RESULT NOT RECEIVED');
    try {
      try {
        await driver.wait(webdriver.until.elementLocated(By.xpath(`//*[contains(@class, 'ownershipPeriods')]//*[contains(text(), 'Периоды владения транспортным средством')][text()]`)), 2000)
          .then(async () => {
            await driver.findElement(By.xpath(`//*[contains(@class, 'ownershipPeriods')]//*[contains(text(), 'Периоды владения транспортным средством')][text()]`));
            await console.log('HISTORY CHECKING RESULT IS FOUND');
            await ChangeTaskItemValue2(taskDB, id, vin, 'collected', 'history');
          });
      } catch (e) {
        await console.log('HISTORY STANDARD RESULT NOT FOUND');
      }
      try {
        await driver.wait(webdriver.until.elementLocated(By.xpath(`//*[@id='checkAutoRestricted']//*[contains(text(),'По указанному VIN (номер кузова или шасси) не найдена')]`)), 2000)
          .then(async () => {
            await driver.findElement(By.xpath(`//*[@id='checkAutoRestricted']//*[contains(text(),'По указанному VIN (номер кузова или шасси) не найдена')]`));
            await console.log('RESTRICTIONS CHECKING RESULT IS FOUND');
            await ChangeTaskItemValue2(taskDB, id, vin, 'collected', 'restrictions');
            return;
          });
      } catch (e) {
        await console.log('RESTRICTIONS STANDARD RESULT NOT FOUND');
      }
    } catch (e) {
      await ChangeTaskItemValue(taskDB, id, vin, 'error', 'OTHER ERROR');
      await console.log('OTHER ERROR');
    }
  }
}

async function ClickToButtonAndCheckData (driver, taskDB, id, vin, button) {
  await ClickToObject(driver, button);
  await ChangeTaskItemValue(taskDB, id, vin, 'waiting');
  await WaitWhileDisplayed(driver, `//*[contains(@class, 'check-message')]//*[contains(text(), 'Выполняется запрос, ждите...')]`, true, 'REQUEST IN PROGRESS, WAIT...', 'REQUEST IS COMPLETE');
  try {
    await driver.wait(webdriver.until.elementLocated(By.xpath(`//*[contains(text(), 'Проверка проведена')]`)), 2500).then(async () => {
      await console.log('CHECKING DATE IS VISIBLE, HAVE RESULT');
      await ChangeTaskItemValue(driver, id, vin, 'receiving');
    });
  } catch (e) {
    await console.log('CHECKING DATE NOT VISIBLE');
    try {
      await driver.wait(webdriver.until.elementLocated(By.xpath(`//*[contains(text(), 'Проверка завершилась ошибкой на стороне сервера')]`)), 2500).then(async () => {
        let errorText = await driver.findElement(By.xpath(`//*[contains(text(), 'Проверка завершилась ошибкой на стороне сервера')]`)).getText();
        await console.log('CHECKING ERROR: ', await errorText);
        await ChangeTaskItemValue2(taskDB, id, vin, 'error', 'error', await errorText);
      });
    } catch (e) {
      await console.log('ERROR, NOT VISIBLE');
      try {
        await driver.wait(webdriver.until.elementLocated(By.xpath(`//*[contains(text(), 'Google reCaptcha v3 не была пройдена')]`)), 2500).then(async () => {
          let errorText = await driver.findElement(By.xpath(`//*[contains(text(), 'Google reCaptcha v3 не была пройдена')]`)).getText();
          await console.log('CAPTCHA NOT SOLVED: ', await errorText);
          await ChangeTaskItemValue2(taskDB, id, vin, 'error', 'error', await errorText);
        });
      } catch (e) {
        await console.log('CAPTCHA ERROR, NOT VISIBLE');
      }
    }
  }
}

async function RandomNumber (min, max) {
  let rand = min - 0.5 + Math.random() * (max - min + 1);
  return Math.round(rand);
}

async function DisplayedChecking (driver, xpath, taskData) {
  const { taskDB, vinTaskDB, id, vin, status, type, } = taskData;

  try {
    await driver.wait(until.elementLocated(By.xpath(xpath)), 2000).then(async () => {
      if (!!await driver.findElement(By.xpath(xpath)).isDisplayed()) {
        let errorText = await driver.findElement(By.xpath(xpath)).getText();
        if (!!taskData) {
          await ChangeTaskItemValue2((!!taskDB) ? taskDB : vinTaskDB, id, vin, status, type, await errorText);
        }
      }
    });
    return;
  } catch (e) {
    await console.log('VIN or GOS NUMBER IS VALID');
  }
}

async function WaitWhileFalse (variable, timeout = 1500) {
  do {
    await console.log('WAITING');
    await Sleep(timeout);
  } while (!variable);
}

async function WaitWhileTrue (variable, timeout = 1500) {
  do {
    await console.log('WAITING');
    await Sleep(timeout);
  } while (!!variable);
}

async function GetElementsLength (driver, xpath) {
  let arrayLength;

  await driver.wait(until.elementLocated(By.xpath(xpath)), 3000).then(async element => {
    await ScrollToElement(driver, xpath);
    await driver.wait(until.elementIsEnabled(element)).then(async () => {
      await driver.findElements(By.xpath(xpath)).then(async elements => {
        await elements.map(async (item, index, arr) => {
          arrayLength = await arr.length;
        });
      });
    });
  });
  return await arrayLength;
}

module.exports.SendValueToField = SendValueToField;
module.exports.SendToAttributeValue = SendToAttributeValue;
module.exports.ClickToObject = ClickToObject;
module.exports.WaitWhileNotDisplayed = WaitWhileNotDisplayed;
module.exports.WaitWhileDisplayed = WaitWhileDisplayed;
module.exports.WaitElementLocated = WaitElementLocated;
module.exports.DeleteElementFromDOM = DeleteElementFromDOM;
module.exports.GetDataToArray = GetDataToArray;
module.exports.ScrollToElement = ScrollToElement;
module.exports.TakeElementScreensShot = TakeElementScreensShot;
module.exports.ChangeTaskItemValue = ChangeTaskItemValue;
module.exports.ChangeTaskItemValue2 = ChangeTaskItemValue2;
module.exports.Sleep = Sleep;
module.exports.CreateCollectionID = CreateCollectionID;
module.exports.FirstDriverAction = FirstDriverAction;
module.exports.ClickingOnButton = ClickingOnButton;
module.exports.RandomNumber = RandomNumber;
module.exports.WaitWhileFalse = WaitWhileFalse;
module.exports.WaitWhileTrue = WaitWhileTrue;
module.exports.GetElementsLength = GetElementsLength;
