const ProxyGroup = [
  {
    address: '94.242.54.130:28853',
    login: 'TIGscXVr',
    password: '4RzoECsi'
  },
  {
    address: '94.242.54.130:25466',
    login: 'n5nPaSYV',
    password: 'tjSMl5ct'
  },
  {
    address: '94.242.54.130:25431',
    login: 'shyII9OO',
    password: 'SCd2EQNg'
  },
  {
    address: '94.242.54.139:25427',
    login: '06fz7v5G',
    password: 'XvZt6AJp'
  },
  {
    address: '94.242.54.130:25428',
    login: 'eaX2Ianv',
    password: 'hYTQnVLB'
  },
  {
    address: '94.242.54.139:25428',
    login: 'lTkOPeGq',
    password: 'BdHofmm9'
  },
  {
    address: '94.242.54.130:25467',
    login: 'ecXXMYpC',
    password: 'gy8vzk7Z'
  },
  {
    address: '94.242.54.129:15834',
    login: 'ooEXcG1D',
    password: 'mY5lH77F'
  },
  {
    address: '94.242.54.139:25466',
    login: '2GSpraon',
    password: 'Ell0yuLI'
  },
  {
    address: '94.242.54.139:21640',
    login: 'PNIqNeU9',
    password: '0Sb6izSj'
  },
  {
    address: '94.242.54.126:20699',
    login: 'pOeuMo2u',
    password: 'jDi8DFMK'
  },
  {
    address: '94.242.54.129:20700',
    login: 'G7fbY1z6',
    password: 'Zd9fV9OX'
  },
  {
    address: '94.242.54.126:20700',
    login: 'qFct4xRq',
    password: 'sBxt1O91'
  },
  {
    address: '94.242.54.129:20701',
    login: 'I67PimBz',
    password: 'c7JI0xnO'
  },
  {
    address: '94.242.54.126:20701',
    login: '60Wvhj8E',
    password: 'gs3gbFyS'
  },
  {
    address: '94.242.54.129:20702',
    login: 'YZoJOTYQ',
    password: 'qG7Nj7Fs'
  },
  {
    address: '94.242.54.126:20702',
    login: 'x2iAnl58',
    password: 'ebLpZ7fC'
  },
  {
    address: '94.242.54.129:20703',
    login: 'BsQZBaRA',
    password: 'V9mwBNMZ'
  },
  {
    address: '94.242.54.126:20703',
    login: 'XIK3S3lX',
    password: 'e8wm9xhz'
  },
  {
    address: '94.242.54.129:20704',
    login: '8TOZ51R1',
    password: 'jWC8hEdh'
  },
  {
    address: '94.242.54.126:20696',
    login: 'ShVluhhP',
    password: 'rGkDkpXT'
  },
  {
    address: '94.242.54.129:20696',
    login: 'uJg8wpZ5',
    password: '7HrqGuoH'
  },
  {
    address: '94.242.54.126:20695',
    login: 'jPIVFqAB',
    password: 'lkG2mEoa'
  },
];

const SpecialProxy = [
  {
    address: '94.242.54.129:20699',
    login: 'VPwe3jlz',
    password: 'vIuXsFax'
  },
  {
    address: '94.242.54.126:20698',
    login: 'oaEN74l0',
    password: 'zSFKt9qQ'
  },
  {
    address: '94.242.54.129:20698',
    login: 'tHn0GMnD',
    password: 'Yi4OHN1c'
  },
  {
    address: '94.242.54.126:20697',
    login: 'd13PqBqQ',
    password: 'sl36VWYQ'
  },
  {
    address: '94.242.54.129:20697',
    login: '6kEY3Z7b',
    password: 'k4YxT1t5'
  },
];

module.exports.ProxyGroup = ProxyGroup;
module.exports.SpecialProxy = SpecialProxy;
