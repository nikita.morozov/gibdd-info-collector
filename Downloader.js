const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');
const http = require('http');
const https = require('https');
const URL = require('url');
const ProgressBar = require('progress');
const EventEmitter = require('events');

class Downloader extends EventEmitter {
  get (url, fileName, fileFormat, outDir) {
    const self = this;

    const rule = /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&:/~\+#]*[\w\-\@?^=%&/~\+#])?/;
    const reg = new RegExp(rule, 'i');
    if (!reg.test(url)) {
      const err = new Error('THE URL IS INVALID');
      // self.emit('error', err);
      throw err;
    }

    const client = url.startsWith('https') ? https : http;
    const opts = {
      host: URL.parse(url).hostname,
      port: URL.parse(url).port,
      path: URL.parse(url).pathname
    };
    client.get(opts, async (res) => {
      if (res.statusCode === 200) {
        const output = outDir || './';
        if (!fs.existsSync(output)) {
          mkdirp.sync(output);
        }
        let nameOfFile = (!fileName) ? path.basename(URL.parse(url).pathname) : fileName;
        if (nameOfFile.length <= 0) {
          nameOfFile = '111';
          const ext = res.headers['content-type'] ? res.headers['content-type'].split('/').pop() : undefined;
          if (ext) {
            nameOfFile += `.${ext}`;
          }
        }
        const dst = path.join(output, (!fileFormat) ? nameOfFile : nameOfFile + `.${fileFormat}`);
        if (fs.existsSync(dst)) {
          fs.unlinkSync(dst);
        }
        const totalLength = parseInt(res.headers['content-length'], 10);
        const ws = fs.createWriteStream(dst, {
          flags: 'a',
          defaultEncoding: 'binary',
        });
        // progress bar
        const barStyle = '[:bar] :current/:total :percent '
          + ':rate/bps :elapsed';
        console.log();
        const bar = new ProgressBar(barStyle, {
          complete: '=',
          incomplete: '-',
          width: 60,
          total: totalLength,
        });
        res.on('data', async (chunk) => {
          ws.write(chunk);
          bar.tick(chunk.length, '');
        });
        res.on('end', async () => {
          ws.end();
          console.log(`\nFILE HAS BEEN DOWNLOADED AT: ${dst}\n`);
          self.emit('done', dst);
        });
      } else {
        await console.log(`REQUEST FAILED! STATUS CODE: ${res.statusCode}`);
        await this.get(url, fileName, fileFormat, outDir);
        // throw new Error(`REQUEST FAILED! STATUS CODE: ${res.statusCode}`);
      }
    });
  }
}

module.exports = Downloader;
